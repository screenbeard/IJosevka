# IJosevka

A custom version of the Iosevka font for my own use.

This version uses JetBrains Mono as the "base" (Iosevka offers many variants of characters and bundles some of them as starting points that share features with other fonts). JetBrains uses a tall x-height, giving the characters something like a popcorn feeling, they're tight and compact and cute. IJosevka contains changes to the x-height and cap height to simulate that pocorn feeling. The difference is subtle - the version on the right shows the new version.
![](iJosevka-x-height.png)

On top of that I've also made some discretionary changes to a few characters that I just like better this way.

After building a custom private-build-plans.toml file from [the Iosevka customiser](https://typeof.net/Iosevka/customizer), run `make iosevka` followed by `make install` to put the fonts in `~/.fonts/IJosevka`.

