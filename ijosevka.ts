#!/usr/bin/env -S deno run --allow-env --allow-read --allow-write --allow-run
import Sade from "https://cdn.deno.land/sade/versions/v1.8.1/raw/deno/mod.js";

const prog = Sade('ijosevka');

prog.version("1.0.0")
    .describe("A commandline builder of the IJosevka font set");

prog.command("build")
    .describe("Build the IJosevka container")
    .action(buildHandler);
prog.command("daily")
    .describe("Build the quasi-proportional font for \"daily\" usage.")
    .action(buildDaily);
prog.command("terminal")
    .describe("Build the terminal iJosevka font.")
    .action(buildTerminal);
prog.command("web")
    .describe("Build the terminal iJosevka font.")
    .action(buildWeb);
prog.command("main")
    .describe("Build the full iJosevka font.")
    .action(buildMain);
prog.command("install [location]")
    .describe("install fonts to <location>/IJosevka - defaults to $HOME/.fonts/")
    .action(install);

prog.parse(Deno.args);

async function buildHandler(){
    const p = Deno.run({ cmd: ["podman", "build", "--no-cache", "-t", "iosevka_builder", ".", "-f", "Containerfile"]});

    await p.status();
}

async function buildDaily(): Promise<void>{
    const p = Deno.run({ cmd: ["podman", "run", "-it", "-v", `${Deno.cwd()}/dist:/build/dist`,
		"-v", `${Deno.cwd()}/private-build-plans.toml:/build/private-build-plans.toml`,
		"iosevka_builder", "npm", "run", "build", "--", "super-ttc::i-josevka-daily"
    ]});

    await p.status();
}

async function buildTerminal(): Promise<void>{
    console.log(`> Building the terminal version of IJosevka`);
    const p = Deno.run({ cmd: ["podman", "run", "-it", "-v", `${Deno.cwd()}/dist:/build/dist`,
		"-v", `${Deno.cwd()}/private-build-plans.toml:/build/private-build-plans.toml`,
		"iosevka_builder", "npm", "run", "build", "--", "super-ttc::i-josevka-terminal"
    ]});

    await p.status();
    console.log(`> Complete`);
}

async function buildWeb(): Promise<void>{
    console.log(`> Building the web version of IJosevka`);
    const p = Deno.run({ cmd: ["podman", "run", "-it", "-v", `${Deno.cwd()}/dist:/build/dist`,
		"-v", `${Deno.cwd()}/private-build-plans.toml:/build/private-build-plans.toml`, "iosevka_builder", "npm", "run", "build", "--", "webfont::i-josevka"
    ]});

    await p.status();
    console.log(`> Complete`);
}
async function buildMain(): Promise<void>{
    console.log(`> Building the main version of IJosevka`);
    const p = Deno.run({ cmd: ["podman", "run", "-it", "-v", `${Deno.cwd()}/dist:/build/dist`,
		"-v", `${Deno.cwd()}/private-build-plans.toml:/build/private-build-plans.toml`, "iosevka_builder", "npm", "run", "build", "--", "super-ttc::i-josevka"
    ]});

    await p.status();
    console.log(`> Complete`);
}

async function install(location: string|undefined): Promise<void>{
    const base_dir = location ?? `${Deno.env.get("HOME")}/.fonts`;
    const base_info = Deno.lstatSync(base_dir);
    if (!base_info.isDirectory) { throw new Error(`${base_dir} is not a directory`); }
    Deno.removeSync(`${base_dir}/IJosevka`, { recursive: true });
    Deno.mkdirSync(`${base_dir}/IJosevka`); 

    const copy_promises = [];
    for await (const ttc of Deno.readDir("dist/.super-ttc")) {
        if (ttc.isFile){
            copy_promises.push(Deno.copyFile(`dist/.super-ttc/${ttc.name}`, `${base_dir}/IJosevka/${ttc.name}`));
        }
    }
    await Promise.all(copy_promises);
        
    const p = Deno.run(
        { cmd: ["fc-cache", "-f", "-v"]}
    );
    await p.status();
}
